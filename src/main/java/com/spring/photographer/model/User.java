package com.spring.photographer.model;


import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "User")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id", nullable = false)
    private int userId;

    @Column(name = "first_name", nullable = false)
    @Size(min = 3, max = 255)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    @Size(min = 3, max = 255)
    private String lastName;

    @Column(name = "username", nullable = false)
    @Size(min = 3, max = 255)
    private String username;

    @Column(name = "password", nullable = false)
    @Size(min = 3, max = 255)
    private String password;

    @Column(name = "country", nullable = true)
    private String country;

    @Column(name = "state", nullable = true)
    private String state;

    @Column(name = "city", nullable = true)
    private String city;

    public User() {
    }

    public User(@Size(min = 3, max = 255) String firstName, @Size(min = 3, max = 255) String lastName, @Size(min = 3, max = 255) String username, @Size(min = 3, max = 255) String password, String country, String state, String city) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.country = country;
        this.state = state;
        this.city = city;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
