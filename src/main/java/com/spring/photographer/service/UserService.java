package com.spring.photographer.service;

import com.spring.photographer.model.User;

public interface UserService {
    public User findUserByUsername(String username);

    public void saveUser(User user);
}
